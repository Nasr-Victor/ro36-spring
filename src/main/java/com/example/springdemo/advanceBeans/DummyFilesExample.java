package com.example.springdemo.advanceBeans;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@Configuration
public class DummyFilesExample {

    @Scheduled(fixedDelay = 1000 * 60 * 10)
    public void fileReadWriteExample() {

        try {

            FileReader reader =
                    new FileReader("D:\\Programming\\SDA Course\\Spring\\spring-demo-SDA\\src\\main\\resources\\read.txt"); // aici ai dai si path-ul cu totul

            //file reader citeste litera cu litera , de aia avem o metoda read ,
            // ca sa nu citeasca bit cu bit , il encapsulam cu un bufferReader ,si din el apelam o metoda care sa
            // citeasca o linie intreaga
            BufferedReader bufferedReader = new BufferedReader(reader);

            String firstLine = bufferedReader.readLine();
            System.out.println(firstLine);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
