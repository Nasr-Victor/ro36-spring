package com.example.springdemo.jpa;

import com.example.springdemo.jpa.entity.*;
import com.example.springdemo.jpa.entity.Class;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDate;
import java.util.*;

// nu este o clasa de test, doar o folosim ca sa ne asiguram ca ce facem functioneaza
@Configuration

public class TestDataBase {

    @Autowired
    private ClassRepository classRepository;
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Scheduled(fixedDelay = 1000 * 60)
    public void testDataBase() {

        System.out.println("===== Students =====");

        queryFindAllStudents();

        System.out.println("-----");

        queryFindStudentByFirstName("Ion");

        System.out.println("-----");

        queryFindStudentByNameAndLastName("Denis", "Damian");

        System.out.println("-----");

        findStudentsFromAClass();

        System.out.println("-----");

        queryFindNumberOfStudentsInAClass();

        System.out.println("-----");

        queryFindStudentByDateOfBirth();

        System.out.println("-----");

        queryFindStudentsBetweenTwoBirthDates();

        System.out.println("-----");

        queryFindYoungestStudent();


        System.out.println("===== Classes =====");

        queryFindAllClasses();

        System.out.println("###########");

        queryFindClassByName("2_C");

        System.out.println("###########");

        queryFindClassById(3);

        System.out.println("===== Teachers =====");

        queryFindAllTeachers();

        System.out.println("*****");

        findTeacherByFirstNameAndLastName();

        System.out.println("*****");

        queryFindTeacherById();

        System.out.println("*****");

        queryFindTeacherBySalary();

        System.out.println("*****");

        queryHighestPaidTeacher();

        System.out.println("*****");

        queryFindTeacherBySubject();

        System.out.println("*****");

        queryAvarageSalayForTeachers();

        System.out.println("===== Subjects =====");

        queryFindAllSubjects();

        System.out.println("^^^^^^^");

        queryFindSubjectById();

        System.out.println("^^^^^^^");

        queryFindSubjectByName();


    }

    private void queryFindSubjectByName() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the subject name ou are looking for :");
        String word = input.nextLine();
        Subject subjectName = subjectRepository.findBySubjectName(word);
        if (subjectName == null) {
            System.out.println("No subject found with this name!");
        } else {
            System.out.println(subjectName);
        }

    }

    private void queryFindStudentsBetweenTwoBirthDates() {
        Scanner input = new Scanner(System.in);
        List<Student> students;

        System.out.println("Enter first student date of birth : ");
        String firstDateString = input.nextLine();
        System.out.println("Enter second student date of birth : ");
        String secondDateString = input.nextLine();

        LocalDate firstDate = LocalDate.parse(firstDateString);
        LocalDate secondDate = LocalDate.parse(secondDateString);

        students = studentRepository.findByDateOfBirthBetween(firstDate, secondDate);

        if (students == null) {
            System.out.println("No Studentes between those 2 dates you enterd");
        }

        students.forEach(System.out::println);
    }

    private void queryFindStudentByDateOfBirth() {
        Scanner input = new Scanner(System.in);
        String dateOfBirth = null;
        List<Student> students = null;

        while (dateOfBirth == null) {
            System.out.println("Enter a date of birth for a student : ");
            dateOfBirth = input.nextLine();

//            DateFormat dateFormat = new SimpleDateFormat("yyyy MMM dd");
//            Date date = (Date) dateFormat.parse(dateOfBirth);

            LocalDate date = LocalDate.parse(dateOfBirth);
            students = studentRepository.findByDateOfBirth(date);

            if (students.isEmpty()) {
                System.out.println("No Student with such a birthday");
            }
        }
        students.forEach(System.out::println);
    }

    private void queryFindNumberOfStudentsInAClass() {
        Class classByName = null;
        String className = null;
        while (classByName == null) {
            System.out.println("Enter class name you want to find nr of students in it !");
            Scanner input = new Scanner(System.in);
            className = input.nextLine();

            classByName = classRepository.findByName(className);

            if (classByName == null) {
                System.out.println("The class name you entered is not valid, please try again!");
            }
        }
        Integer numberOfStudentInAClass = studentRepository.countStudentByClassId(classByName.getId());

        System.out.println("Number Of Students in the class : " + className + " is : " + numberOfStudentInAClass);
    }

    private void findStudentsFromAClass() {
        Class studentClass = null;
        while (studentClass == null) {
            System.out.println("Enter a class name : ");
            Scanner input = new Scanner(System.in);
            String className = input.nextLine();

            studentClass = classRepository.findByName(className);

            if (studentClass == null) {
                System.out.println("You introduced invalid class Name");
            }
        }

        List<Student> students = studentRepository.findByClassId(studentClass.getId());
        students.forEach(System.out::println);
    }


    private void queryFindClassById(Integer id) {
        System.out.println("The Class with id : " + id + " is : " + classRepository.findById(id));
    }

    private void queryFindClassByName(String className) {
        System.out.println("The Class with Name : " + className + " is : " + classRepository.findByName(className));
    }

    private void queryFindSubjectById() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a subject Id :");
        Integer id = input.nextInt();
        Optional<Subject> subjectById = subjectRepository.findById(id);

        if (subjectById.isEmpty()) {
            System.out.println("you've entared invalid subject Id");
        } else {
            System.out.println("The Subject with id : "
                    + id + " is : " + subjectById);
        }
    }

    private void queryFindAllSubjects() {
        List<Subject> subjects = subjectRepository.findAll();
        subjects.forEach(System.out::println);
    }

    private void queryFindTeacherBySalary() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the teacher's salary");
        Integer salary = input.nextInt();

        List<Teacher> teachersBySalary = teacherRepository.findTeacherBySalary(salary);

        if (teachersBySalary.isEmpty()) {
            System.out.println("No teacher found with this salary");
        } else {
            System.out.println("The Teacher with the salary : " + salary + " Is : "
                    + teachersBySalary);
        }
    }

    private void queryFindTeacherById() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter teachers Id: ");
        Integer id = input.nextInt();
        Teacher teachersById = teacherRepository.findTeachersById(id);
        if (teachersById == null) {
            System.out.println("You've entared invalid id ");
        } else {
            System.out.println("Teacher By Id : " + teachersById);
        }
    }

    private void findTeacherByFirstNameAndLastName() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter teacher's first name ");
        String firstName = input.nextLine();
        System.out.println("Enter teacher's last name ");
        String lastName = input.nextLine();

        List<Teacher> teachersByFirstNameAndLastName =
                teacherRepository.findTeachersByFirstNameAndLastName(firstName, lastName);

        if (teachersByFirstNameAndLastName.isEmpty()) {
            System.out.println("No teacher found with this name!");
        } else {
            System.out.println("Teacher By First & Last Name : "
                    + teachersByFirstNameAndLastName);
        }
    }


    private void queryFindYoungestStudent() {
        Optional<Student> youngestStudent = studentRepository.findAll().stream()
                .min((student1, student2) -> student1.getDateOfBirth().compareTo(student2.getDateOfBirth()));
        System.out.println("YoungestStudent : " + youngestStudent);
    }

    private void queryHighestPaidTeacher() {
        Optional<Teacher> highestSalaryForTeacher = teacherRepository.findAll()
                .stream().max((salary1, salary2) -> salary1.getSalary().compareTo(salary2.getSalary()));
        System.out.println("Highest paid teacher is : " + highestSalaryForTeacher);
    }


    private void queryAvarageSalayForTeachers() {
        OptionalDouble averageSalaryForTeachers = teacherRepository.findAll()
                .stream().mapToInt(Teacher::getSalary).average();
        System.out.println("Average salary for teachers is : " + averageSalaryForTeachers);
    }

    private void queryFindTeacherBySubject() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the subject needed : ");

        String word = input.nextLine();

        Subject subject = subjectRepository.findBySubjectName(word);

        if (subject == null) {
            System.out.println("No teacher found for this Subject");
        } else {
            List<Teacher> teachersBySubject = teacherRepository.findAllBySubjectId(subject.getId());
            System.out.println("The teacher for " + word + " is : " + teachersBySubject);


        }
    }

    private void queryFindAllTeachers() {
        List<Teacher> teacherList = teacherRepository.findAll();
        teacherList.forEach(System.out::println);
    }

    private void queryFindAllClasses() {
        List<Class> classList = classRepository.findAll();
        classList.forEach(System.out::println);
    }

    private void queryFindAllStudents() {
        List<Student> studentList = studentRepository.findAll();
        studentList.forEach(student -> System.out.println(student.getName() + " " + student.getLastName()));
    }

    //    //ma folosesc de StudentRepository ca sa fac metodele de mai jos

    public void queryFindStudentByFirstName(String firstName) {
        List<Student> queryStudentList = studentRepository.findByName(firstName);
        queryStudentList.forEach(System.out::println);
    }

    public void queryFindStudentByNameAndLastName(String firstName, String lastName) {
        List<Student> queryStudentList2 = studentRepository.findByNameAndLastName(firstName, lastName);
        queryStudentList2.forEach(System.out::println);
    }


}
