package com.example.springdemo.jpa.entity;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Integer> {

    List<Teacher> findTeachersByFirstNameAndLastName(String firstName, String lastName);

    Teacher findTeachersById(Integer id);
    List<Teacher> findAllBySubjectId(Integer id);
    List<Teacher> findTeacherBySalary(Integer salary);

    List<Teacher> findAll();


}
