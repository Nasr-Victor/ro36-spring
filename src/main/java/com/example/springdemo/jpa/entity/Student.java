package com.example.springdemo.jpa.entity;


import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity(name = "Student")
public class Student {

    @Id
    private int id;
    // in general nu trebuie sa punem @Cololmn pentru ca Hibernate stie sa faca asta
    // mai jos este doar ca sa zica ca are nume diferit

    @Column(name = "first_name")
    private String name;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;
    @Column(name = "class_id")
    private Integer classId;
}
