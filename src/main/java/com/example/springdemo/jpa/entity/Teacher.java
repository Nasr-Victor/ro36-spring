package com.example.springdemo.jpa.entity;

import lombok.*;
import lombok.extern.apachecommons.CommonsLog;

import javax.persistence.*;
import javax.security.auth.Subject;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity(name = "Teacher")
public class Teacher {

    @Id
    private int id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "salary")
    private Integer salary;
    @Column(name = "subject_id")
    private Integer subjectId;


}
