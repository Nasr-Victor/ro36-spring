package com.example.springdemo.jpa.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity(name = "subject")
public class Subject {
    @Id
    private Integer id;
    @Column(name = "subject_name")
    private String subjectName;
    @Column(name = "teza")
    private String teza;

}
