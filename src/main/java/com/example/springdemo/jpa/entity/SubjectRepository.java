package com.example.springdemo.jpa.entity;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Integer> {

    Optional<Subject> findById(Integer id);

    List<Subject> findAll();

    Subject findBySubjectName(String name);
}
