package com.example.springdemo.jpa.entity;

import lombok.*;

import javax.persistence.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor



@Entity(name = "class")
public class Class {

    @Id
    private Integer id;

    @Column
    private String name;

}
