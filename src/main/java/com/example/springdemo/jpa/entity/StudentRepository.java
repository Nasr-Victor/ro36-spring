package com.example.springdemo.jpa.entity;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

//annotation for spring to create a bean of SimpleJPARepository<Student,Integer>
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
    // JpaRepository -> hold functionality from  data management.

    // pasam parametru de tip String pentru sa stie ce sa caute ,el in spate transforma metoda asta in select * from ....


    @Override
    List<Student> findAll();

    List<Student> findByName(String firstName);

    List<Student> findByNameAndLastName(String firstName, String lastName);

//    List<Student> findAllByClass_id(Integer classId); //

    List<Student> findByClassId(Integer classId);

    Integer countStudentByClassId(Integer classId);

    List<Student> findByDateOfBirth(LocalDate date);

    List<Student> findByDateOfBirthBetween(LocalDate firstDate, LocalDate secondDate);


}
