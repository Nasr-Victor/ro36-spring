package com.example.springdemo.jpa.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClassRepository extends JpaRepository<Class, Integer> {

    @Override
    List<Class> findAll();

    Optional<Class> findById(Integer id);

    Class findByName(String name);


}
