package com.example.springdemo.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

//@Configuration
public class Scheduler {

    //Bean
    @Autowired
    PrettyPrinter pp;

    //??NotSoPrettyPrinter

    private NotSoPrettyPrintter nspp;

    private UglyPrinter up;

    @Autowired
    public void setUp(UglyPrinter up) {
        this.up = up;
    }

    public Scheduler(NotSoPrettyPrintter nspp) {
        this.nspp = nspp;
    }

    //fixed delay is in seconds
    @Scheduled(fixedDelay = 1000 * 60 * 10)
    public void every10Minutes() {
        pp.prettyPrint("Am ajuns aici");
        nspp.noSoPrettyPrint("***");
        up.uglyPrinter("&&&&");

    }
}
