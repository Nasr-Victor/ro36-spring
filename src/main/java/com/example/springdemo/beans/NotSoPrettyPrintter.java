package com.example.springdemo.beans;

import org.springframework.stereotype.Component;

@Component
public class NotSoPrettyPrintter {

    public void noSoPrettyPrint(String message){
        System.out.println("No So " + message + " No So");
    }
}
