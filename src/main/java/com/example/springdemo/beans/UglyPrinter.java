package com.example.springdemo.beans;

import org.springframework.stereotype.Component;

@Component
public class UglyPrinter {

    public void uglyPrinter(String message) {
        System.out.println("from ugly printer " + message + " Ugly");
    }
}
