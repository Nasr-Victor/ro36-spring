package com.example.springdemo.beans;

import org.springframework.stereotype.Component;

@Component
public class PrettyPrinter {


    public void prettyPrint(String message) {
        System.out.println("---------------");
        System.out.println("| " + message + " |");
        System.out.println("--------------");

    }
}
