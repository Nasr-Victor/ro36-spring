package com.example.springdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringDemoApplication {

    public static void main(String[] args) {
//        PrettyCommunicator p1 = new PrettyCommunicator();
//        p1.prettyPrint("Hello");
        SpringApplication.run(SpringDemoApplication.class, args);
    }

}
