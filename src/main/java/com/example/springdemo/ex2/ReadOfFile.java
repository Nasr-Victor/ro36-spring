package com.example.springdemo.ex2;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@Component("SpecialFile")
public class ReadOfFile  implements GenericReader{

    // tot ce trebuie intializat ,trebuie facuta in constructor
    BufferedReader bufferedReader;

    public ReadOfFile() {
        try {
            FileReader fileReader = new FileReader("D:\\Programming\\SDA Course\\Spring\\spring-demo-SDA\\src\\main\\resources\\read.txt");
            bufferedReader = new BufferedReader(fileReader);

        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }


    public String read() {
        String s = null;
        try {
            s = bufferedReader.readLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return s;
    }


//    Method to read the hole File
//    @Override
//    public void read() {
//        String line = bufferedReader.lines().collect(Collectors.joining());
//
//        System.out.println(line);
//
//    }
}
