package com.example.springdemo.ex2;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class ReadOfKeyBoard implements GenericReader {

    // tot ce trebuie intializat ,trebuie facuta in constructor

    Scanner input;

    public ReadOfKeyBoard() {
        input = new Scanner(System.in);
    }


    public String read() {
        return input.nextLine();
    }
}
