package com.example.springdemo.ex2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

//@Configuration
//@EnableScheduling

public class Schedualed {

//    @Autowired
//    ReadOfFile readOfFile;
//    @Autowired
//    ReadOfKeyBoard readOfKeyBoard;

    @Qualifier("SpecialFile")
    @Autowired
    GenericReader genericReader;

//    @Scheduled(fixedDelay = 1000 * 60 * 1) trebuie sa il decomentezi cand rei sa il folosesti
    public void suprimeReader() {
//        String rf = readOfFile.read();
//        String rk = readOfKeyBoard.read();
//        System.out.println(rf);
//        System.out.println(rk);
//        System.out.println(genericReader.read());
        if(genericReader.getClass().equals(ReadOfKeyBoard.class)){
            System.out.println(ReadOfKeyBoard.class);
        }
    }

}
