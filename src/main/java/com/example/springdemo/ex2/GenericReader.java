package com.example.springdemo.ex2;

public interface GenericReader {
    public String read();

}
