package com.example.springdemo.ex1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

//@Configuration
public class Communicator {

    @Autowired
    private Greatings greatings;

    public Communicator(Greatings gr) {
        this.greatings = gr;
    }


    @Autowired
    private Reader reader;

    public void setReader(Reader key) {
        this.reader = key;
    }

//    @Scheduled(fixedDelay = 1 * 60 * 1000) trebui sa il decomentezi cand vrei sa il folosesti
    public void helloUser() {
        greatings.greate();
        String name = reader.getName();
        greatings.hi(name);
    }

}
