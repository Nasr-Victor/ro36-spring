package com.example.springdemo.ex1;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class Reader {

    Scanner input = new Scanner(System.in);

    public String getName() {
        return input.nextLine();
    }
}
