package com.example.springdemo.web.Controller;

import com.example.springdemo.jpa.entity.Student;
import com.example.springdemo.jpa.entity.StudentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/api/students")
public class StudentController {

    private final StudentRepository repository;

    public StudentController(StudentRepository repository) {
        this.repository = repository;
    }

//    @RequestMapping(
//            method = RequestMethod.GET,
//            // http://localhost:8080/api/students
//            path = "/api/students"
//    )
//    public ResponseEntity<List<Student>> getAllStudents() {
//        List<Student> students = repository.findAll();
//
//        ResponseEntity<List<Student>> response = new ResponseEntity<>(students, HttpStatus.OK);
//
//        return response;
//
//    }

    //    @RequestMapping(
//            method = RequestMethod.GET,
//            path = "/api/studentsByName"
//    )
//    @GetMapping("/api/studentByName")

    @GetMapping()
    public ResponseEntity<List<Student>> getStudentByFirstName(@RequestParam(name = "first_name") String firstName) {
        List<Student> students;
        if (firstName == null) {
            students = repository.findAll();
        } else {
            students = repository.findByName(firstName);
        }

        return ResponseEntity.ok(students);
    }
}
