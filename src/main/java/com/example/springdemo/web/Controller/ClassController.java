package com.example.springdemo.web.Controller;

import com.example.springdemo.jpa.entity.Class;
import com.example.springdemo.jpa.entity.ClassRepository;
import com.example.springdemo.jpa.entity.Student;
import com.example.springdemo.jpa.entity.StudentRepository;
import org.hibernate.boot.archive.scan.spi.ClassFileArchiveEntryHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/api/class")
public class ClassController {
    private final ClassRepository classRepository;
    private final StudentRepository studentRepository;


    public ClassController(ClassRepository classRepository, StudentRepository studentRepository) {
        this.classRepository = classRepository;
        this.studentRepository = studentRepository;
    }

    //returneaza toti studentii dintr-o clasa
    //http://localhost:8080/api/class/12_C/students <- http://localhost:8080 ( din string) + /api/class
    // (de pe controller/class) + /12_C/students (de pe metoda)
    //In URL-uri carcaterul " " (space e inlacuit cu "20%"

    @GetMapping("/{class_name}/students")
    public ResponseEntity getAllStudentsFromClass(@PathVariable("class_name") String className) {

        // Cautam clasaEntity dupa NUME (12_C) ca sa ii aflam (id-ul 2)
        Class classEntity = classRepository.findByName(className);
        if (classEntity == null) {
            return new ResponseEntity("No such class could be found", HttpStatus.BAD_REQUEST);
        }

        Integer classId = classEntity.getId();
        List<Student> students = studentRepository.findByClassId(classId);
        return ResponseEntity.ok(students);
    }
}
