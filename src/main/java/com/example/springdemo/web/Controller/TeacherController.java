package com.example.springdemo.web.Controller;

import com.example.springdemo.jpa.entity.Teacher;
import com.example.springdemo.jpa.entity.TeacherRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class TeacherController {

    private TeacherRepository teacherRepository;

    public TeacherController(TeacherRepository tr) {
        this.teacherRepository = tr;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            path = "/api/Teachers"
    )
    public ResponseEntity<List<Teacher>> getAllTeachers() {
        List<Teacher> teacher = teacherRepository.findAll();

        ResponseEntity<List<Teacher>> response = new ResponseEntity<>(teacher, HttpStatus.OK);

        return response;
    }

}
